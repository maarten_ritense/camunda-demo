## About ##
This project contains a demo application to play around with Camunda BPMN engine.
It uses a PostgreSQL database which runs as a container in docker.  

### Prerequisites ###
- OpenJDK 11
- Docker Desktop

#### Camunda Cockpit ####
- <http://127.0.0.1:8000/camunda/app/welcome/default/#!/login>

#### Camunda REST API ####
- <http://127.0.0.1:8000/camunda/rest/engine>

#### Keycloak ####
- <http://localhost:8080/auth/>

#### H2 Console ####
- <http://127.0.0.1:8000/h2-console> (don't forget to change JDBC url to: jdbc:h2:mem:camunda-demo-dev) 

#### Camunda references ####
- <https://docs.camunda.org/manual/7.15/user-guide/spring-boot-integration/configuration/>
- <https://docs.camunda.org/manual/7.15/reference/bpmn20/subprocesses/call-activity/>
- <https://docs.camunda.org/manual/7.15/reference/bpmn20/events/message-events/>

#### Camunda best practices ####
- <https://camunda.com/best-practices/_/>
- <https://camunda.com/best-practices/dealing-with-problems-and-exceptions/#knowing-typical-dos-and-donts-for-save-points>

#### Camunda testing resources ####
- <https://github.com/camunda/camunda-bpm-assert>
- <https://github.com/camunda/camunda-bpm-assert-scenario>
- <https://github.com/camunda/camunda-bpm-mockito>
- <https://github.com/camunda/camunda-bpm-process-test-coverage>

#### Keycloak ####
- <https://www.keycloak.org/documentation>
- <https://github.com/keycloak/keycloak-containers>
- <https://github.com/keycloak/keycloak-containers/blob/14.0.0/server>
- <https://quay.io/repository/keycloak/keycloak?tab=tags>

#### Other useful resources ####
- <https://camunda.com/download/modeler/>
- <https://asyncstream.com/tutorials/camunda-bpmn-testing-coverage/>
- <https://github.com/avast/gradle-docker-compose-plugin>
