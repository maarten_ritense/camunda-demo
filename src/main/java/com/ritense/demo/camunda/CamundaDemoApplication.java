package com.ritense.demo.camunda;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@Slf4j
@SpringBootApplication
public class CamundaDemoApplication {

    /**
     * Main method, used to run the application.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(CamundaDemoApplication.class);
        Environment environment = app.run(args).getEnvironment();
        log.info(
            "\n\n----------------------------------------------------------\n" +
            "\tApplication '{}' is running!\n" +
            "\tLocal accessible via: http://127.0.0.1:{}\n" +
            "----------------------------------------------------------\n",
            environment.getProperty("spring.application.name"),
            environment.getProperty("server.port")
        );
    }
}
