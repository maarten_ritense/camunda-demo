package com.ritense.demo.camunda.event.listener;

import lombok.extern.slf4j.Slf4j;
//import org.camunda.bpm.engine.delegate.DelegateExecution;
//import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.impl.history.event.HistoryEvent;
import org.camunda.bpm.spring.boot.starter.event.ExecutionEvent;
import org.camunda.bpm.spring.boot.starter.event.TaskEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CamundaEventListener {

//    @EventListener
//    public void onTaskEvent(DelegateTask taskDelegate) {
//        log.info(
//            "Handle mutable task event '{}' in process-instance '{}'",
//            taskDelegate.getId(),
//            taskDelegate.getProcessInstanceId()
//        );
//    }

    @EventListener
    public void onTaskEvent(TaskEvent taskEvent) {
        log.info(
            "Handle immutable task event '{}' in process-instance '{}'",
            taskEvent.getId(),
            taskEvent.getProcessInstanceId()
        );
    }

//    @EventListener
//    public void onExecutionEvent(DelegateExecution executionDelegate) {
//        log.info(
//            "Handle mutable execution event '{}' in process-instance '{}'",
//            executionDelegate.getCurrentActivityId(),
//            executionDelegate.getProcessInstanceId()
//        );
//    }

    @EventListener
    public void onExecutionEvent(ExecutionEvent executionEvent) {
        log.info(
            "Handle immutable execution event '{}' in process-instance '{}'",
            executionEvent.getCurrentActivityId(),
            executionEvent.getProcessInstanceId()
        );
    }

    @Async
    @EventListener
    public void onHistoryEvent(HistoryEvent historyEvent) {
        log.info(
            "Handle history event '{}' for process-instance '{}'",
            historyEvent.getClass().getSimpleName(),
            historyEvent.getProcessInstanceId()
        );
    }
}
