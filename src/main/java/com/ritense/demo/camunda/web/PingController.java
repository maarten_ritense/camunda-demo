package com.ritense.demo.camunda.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class PingController {

    @GetMapping("/ping")
    public String respondOnPing() {
        return "Pong! The time is now: " + LocalDateTime.now().toString();
    }

}
