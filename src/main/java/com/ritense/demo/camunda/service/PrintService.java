package com.ritense.demo.camunda.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PrintService {

    public void printAgeCategory(DelegateExecution execution) {
        String ageCategory = (String) execution.getVariable("age-category");

        log.info("Age category '{}'", ageCategory);
    }
}
