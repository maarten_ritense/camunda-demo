package com.ritense.demo.camunda.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PrintHelloWorldDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {
        log.info("Hello World!");
    }
}
