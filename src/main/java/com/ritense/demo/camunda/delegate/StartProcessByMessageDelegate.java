package com.ritense.demo.camunda.delegate;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component
public class StartProcessByMessageDelegate implements JavaDelegate {

    public static final String START_SIMPLE_PROCESS_MESSAGE = "start-simple-process-message";

    private final RuntimeService runtimeService;

    public StartProcessByMessageDelegate(RuntimeService runtimeService) {
        this.runtimeService = runtimeService;
    }

    @Override
    public void execute(DelegateExecution execution) {
        runtimeService.createMessageCorrelation(START_SIMPLE_PROCESS_MESSAGE)
            .processInstanceBusinessKey(execution.getBusinessKey())
            .correlateStartMessage();
    }
}
