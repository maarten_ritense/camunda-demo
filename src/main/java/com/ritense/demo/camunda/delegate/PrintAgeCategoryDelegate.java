package com.ritense.demo.camunda.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.Expression;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PrintAgeCategoryDelegate implements JavaDelegate {

    public static final String AGE_CATEGORY_UNDEFINED = "AGE_CATEGORY_UNDEFINED";

    private Expression name;

    @Override
    public void execute(DelegateExecution execution) {
        String ageCategory = (String) execution.getVariable("age-category");

        if (ageCategory == null) {
            log.warn("Age category is undefined!");
            throw new BpmnError(AGE_CATEGORY_UNDEFINED);
        } else {
            log.info("Age category '{}'", ageCategory);
        }
    }
}
