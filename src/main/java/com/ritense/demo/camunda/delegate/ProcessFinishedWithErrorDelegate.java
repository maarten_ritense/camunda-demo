package com.ritense.demo.camunda.delegate;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ProcessFinishedWithErrorDelegate implements JavaDelegate {

    public static final String MESSAGE_NAME = "an-error-occurred-message";
    public static final String NO_PROCESS_INSTANCE_FOUND_WAITING_FOR_MESSAGE = "NO_PROCESS_INSTANCE_FOUND_WAITING_FOR_MESSAGE";

    private final RuntimeService runtimeService;

    public ProcessFinishedWithErrorDelegate(RuntimeService runtimeService) {
        this.runtimeService = runtimeService;
    }

    @Override
    public void execute(DelegateExecution execution) {
        Optional.ofNullable(runtimeService.createExecutionQuery()
            .messageEventSubscriptionName(MESSAGE_NAME)
            .singleResult()
        ).ifPresentOrElse(exec -> runtimeService.messageEventReceived(MESSAGE_NAME, exec.getId()), () -> {
            throw new BpmnError(NO_PROCESS_INSTANCE_FOUND_WAITING_FOR_MESSAGE);
        });
    }
}
