package com.ritense.demo.camunda.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${camunda.bpm.admin-user.id}")
    private String camundaBpmAdminUserId;

    @Value("${camunda.bpm.admin-user.password}")
    private String camundaBpmAdminUserPwd;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS) // Disable sessions
            .and()
            .httpBasic() // Enable basic authentication
            .and()
            .authorizeRequests()
                .antMatchers("/camunda/app/**", "/camunda/api/**") // Camunda Webapps
                    .permitAll() // Handles it's own security
                .antMatchers("/camunda/rest/**"). // Camunda REST API
                    hasRole("ADMIN") // Use in memory user
                .antMatchers("/ping")
                    .permitAll() // no authentication required
            .and()
            .csrf()
                .disable()
            .formLogin()
                .disable();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        // create in memory admin user for specified user and pwd
        auth.inMemoryAuthentication()
            .withUser(camundaBpmAdminUserId)
            .password(passwordEncoder().encode(camundaBpmAdminUserPwd))
            .roles("ADMIN");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
