package com.ritense.demo.camunda;

import com.ritense.demo.camunda.delegate.PrintAgeCategoryDelegate;
import com.ritense.demo.camunda.delegate.PrintHelloWorldDelegate;
import com.ritense.demo.camunda.delegate.ProcessFinishedDelegate;
import com.ritense.demo.camunda.delegate.StartProcessByMessageDelegate;
import com.ritense.demo.camunda.service.PrintService;
import org.assertj.core.api.Assertions;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.RequiredHistoryLevel;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.junit5.test.ProcessEngineExtension;
import org.camunda.bpm.scenario.ProcessScenario;
import org.camunda.bpm.scenario.Scenario;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RequiredHistoryLevel(ProcessEngineConfiguration.HISTORY_NONE)
@Deployment(resources = {
    "bpmn/simple-process.bpmn",
    "bpmn/simple-process-with-start-message.bpmn",
    "bpmn/simple-sub-process.bpmn",
    "bpmn/age-category.dmn"
})
@ExtendWith(ProcessEngineExtension.class)
public class SimpleProcessTest {

    @Mock
    private ProcessScenario simpleProcess;

    @Mock
    private ProcessScenario simpleSubProcess;

    @Mock
    private PrintService printService;

    @Mock
    private PrintAgeCategoryDelegate printAgeCategoryDelegate;

    @Mock
    private PrintHelloWorldDelegate printHelloWorldDelegate;

    @Mock
    private StartProcessByMessageDelegate startProcessByMessageDelegate;

    @Mock
    private ProcessFinishedDelegate processFinishedDelegate;

    private AutoCloseable closeable;

    @BeforeEach
    public void setupMocks() {
        closeable = MockitoAnnotations.openMocks(this);

        Mocks.register("printService", printService);
        Mocks.register("printAgeCategoryDelegate", printAgeCategoryDelegate);
        Mocks.register("printHelloWorldDelegate", printHelloWorldDelegate);
        Mocks.register("startProcessByMessageDelegate", startProcessByMessageDelegate);
        Mocks.register("processFinishedDelegate", processFinishedDelegate);
    }

    @AfterEach
    public void cleanupMocks() throws Exception {
        closeable.close();
    }

    @Test
    public void happyPathTest() {
        // given
        when(simpleProcess.waitsAtUserTask("provide-personal-details"))
            .thenReturn(task -> task.complete(Map.of("fullname", "John Doe", "age", 40L)));

        when(simpleProcess.runsCallActivity("start-simple-sub-process"))
            .thenReturn(Scenario.use(simpleSubProcess));

        when(simpleProcess.waitsAtReceiveTask("continue-after-receiving-message"))
            .thenReturn(receiveTask -> {
                Assertions.assertThat(receiveTask.getEventType()).isEqualTo("message");
                Assertions.assertThat(receiveTask.getEventName()).isEqualTo("continue-process-message");
                receiveTask.receive();
            });

        // when
        Scenario scenario = Scenario.run(simpleProcess)
            .startByKey("simple-process")
            .execute();

        // then
        assertThat(scenario.instance(simpleProcess)).variables().hasSize(3).containsEntry("age-category", "adult");
        assertThat(scenario.instance(simpleProcess)).calledProcessInstance("simple-sub-process");

        verify(simpleProcess).hasCompleted("provide-personal-details");
        verify(simpleProcess).hasFinished("determine-age-category");
        verify(simpleProcess).hasFinished("start-simple-sub-process");
        verify(simpleProcess).hasFinished("start-process-by-message");
        verify(simpleProcess).hasFinished("continue-after-receiving-message");
        verify(simpleProcess).hasFinished("end-event");
    }
}
